//
// Priest.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 18:27:18 2014 Jean Gravier
// Last update Fri Jan 17 09:36:03 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Character.hh"
#include "Mage.hh"
#include "Priest.hh"

Priest::Priest(std::string const& name, int lvl): Mage(name, lvl)
{
  this->_class = "Priest";
  this->_race = "Orc";
  this->_strength = 4;
  this->_stamina = 4;
  this->_intelligence = 42;
  this->_spirit = 21;
  this->_agility = 2;
  std::cout << this->_name << " enters in the order" << std::endl;
}

Priest::~Priest()
{

}

int		Priest::CloseAttack()
{
  if (this->_power >= 10)
    {
      this->_power -= 10;
      std::cout << this->_name << " uses a spirit explosion" << std::endl;
      return (10 + this->_spirit);
    }
  else
    std::cout << this->_name << " out of power" << std::endl;
  return (0);
}

void		Priest::Heal()
{
  if (this->_power >= 10)
    {
      if ((this->_pv + 70) > 100)
	this->_pv = 100;
      else
	this->_pv += 70;
      this->_power -= 10;
      std::cout << this->_name << " casts a little heal spell" << std::endl;
    }
}

