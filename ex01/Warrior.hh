//
// Warrior.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 15:29:51 2014 Jean Gravier
// Last update Thu Jan 16 22:10:39 2014 Jean Gravier
//

#ifndef _WARRIOR_H_
#define _WARRIOR_H_

#include "Character.hh"
#include <string>
#include <iostream>

class		Warrior : public Character
{
public:
  Warrior(std::string const&, int, std::string = "hammer");
  ~Warrior();

public:
  int		CloseAttack();
  int		RangeAttack();

public:
  std::string	getWeaponName() const;

private:
  std::string	_weaponName;
};

#endif /* _WARRIOR_H_ */
