//
// Paladin.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex03
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 19:21:15 2014 Jean Gravier
// Last update Fri Jan 17 09:38:13 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Paladin.hh"

Paladin::Paladin(std::string const& name, int lvl): Character(name, lvl), Warrior(name, lvl), Mage(name, lvl) , Priest(name, lvl)
{
  this->_class = "Paladin";
  this->_race = "Humain";
  this->_strength = 9;
  this->_stamina = 10;
  this->_intelligence = 10;
  this->_spirit = 10;
  this->_agility = 2;
  std::cout << "the light falls on " << this->_name << std::endl;
}

Paladin::~Paladin()
{

}

int		Paladin::CloseAttack()
{
  return (Warrior::CloseAttack());
}

int		Paladin::RangeAttack()
{
  return (Priest::RangeAttack());
}

void		Paladin::Heal()
{
  Priest::Heal();
}

void		Paladin::RestorePower()
{
  Warrior::RestorePower();
}

int		Paladin::Intercept()
{
  return (0);
}
