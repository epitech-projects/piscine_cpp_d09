//
// Priest.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 18:27:21 2014 Jean Gravier
// Last update Thu Jan 16 23:33:41 2014 Jean Gravier
//

#ifndef _PRIEST_H_
# define _PRIEST_H_

#include "Mage.hh"
#include <string>

class		Priest: virtual public Mage
{
public:
  Priest(std::string const&, int);
  ~Priest();

public:
  int		CloseAttack();
  void		Heal();
};

#endif /* _PRIEST_H_ */
