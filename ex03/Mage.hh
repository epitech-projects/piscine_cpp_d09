//
// Mage.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 17:52:31 2014 Jean Gravier
// Last update Thu Jan 16 23:46:06 2014 Jean Gravier
//

#ifndef _MAGE_H_
# define _MAGE_H_

#include <string>
#include "Character.hh"

class		Mage : virtual public Character
{
public:
  Mage(std::string const&, int);
  ~Mage();

public:
  int	CloseAttack();
  int		RangeAttack();
  void		RestorePower();
};

#endif /* _MAGE_H_ */
