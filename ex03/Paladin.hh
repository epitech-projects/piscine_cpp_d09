//
// Paladin.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex03
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 19:21:20 2014 Jean Gravier
// Last update Fri Jan 17 09:38:30 2014 Jean Gravier
//

#ifndef _PALADIN_H_
# define _PALADIN_H_

#include <string>
#include "Warrior.hh"
#include "Priest.hh"

class		Paladin : virtual public Warrior, public Priest
{
public:
  Paladin(std::string const&, int);
  ~Paladin();

public:
  int		CloseAttack();
  int		RangeAttack();
  void		Heal();
  void		RestorePower();
  int		Intercept();
};

#endif /* _PALADIN_H_ */
