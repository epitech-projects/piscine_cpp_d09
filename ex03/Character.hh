//
// Character.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 11:04:14 2014 Jean Gravier
// Last update Thu Jan 16 23:05:22 2014 Jean Gravier
//

#ifndef CHARACTER_HH_
#define CHARACTER_HH_

#include <string>

class			Character
{
public:
  Character(std::string const&, int);
  virtual ~Character();

public:
  std::string const&	getName() const;
  int			getLvl() const;
  int			getPv() const;
  int			getPower() const;
  int			getStrength() const;
  int			getStamina() const;
  int			getIntelligence() const;
  int			getSpirit() const;
  int			getAgility() const;
  std::string		getRace() const;
  std::string		getClass() const;

public:
  virtual int		CloseAttack();
  virtual int		RangeAttack();
  virtual void		Heal();
  virtual void		RestorePower();
  void			TakeDamage(int);

public:
  typedef enum { CLOSE, RANGE } AttackRange;
  AttackRange		Range;

protected:
  std::string		_name;
  std::string		_race;
  std::string		_class;
  int			_lvl;
  int			_pv;
  int			_power;
  int			_strength;
  int			_stamina;
  int			_intelligence;
  int			_spirit;
  int			_agility;
};

#endif /* !CHARACTER_HH_ */
