//
// Warrior.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 15:29:51 2014 Jean Gravier
// Last update Thu Jan 16 22:22:59 2014 Jean Gravier
//

#ifndef _WARRIOR_H_
#define _WARRIOR_H_

#include "Character.hh"
#include <string>
#include <iostream>

class		Warrior : virtual public Character
{
public:
  Warrior(std::string const&, int, std::string = "hammer");
  ~Warrior();

public:
  int		CloseAttack();
  int		RangeAttack();

public:
  std::string	getWeaponName() const;

protected:
  std::string	_weaponName;
};

#endif /* _WARRIOR_H_ */
