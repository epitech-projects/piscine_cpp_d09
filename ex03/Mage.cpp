//
// Mage.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 17:59:45 2014 Jean Gravier
// Last update Thu Jan 16 22:30:19 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Character.hh"
#include "Mage.hh"

Mage::Mage(std::string const& name, int lvl): Character(name, lvl)
{
  this->_class = "Mage";
  this->_race = "Gnome";
  this->_strength = 6;
  this->_stamina = 6;
  this->_intelligence = 12;
  this->_spirit = 11;
  this->_agility = 7;
  std::cout << this->_name << " teleported" << std::endl;
}

Mage::~Mage()
{

}

int		Mage::CloseAttack()
{
  this->Range = Character::CLOSE;
  if (this->_power >= 10)
    {
      this->_power -= 10;
      std::cout << this->_name << " blinks" << std::endl;
      return (0);
    }
  else
    std::cout << this->_name << " out of power" << std::endl;
  return (0);
}

int		Mage::RangeAttack()
{
  if (this->_power >= 25)
    {
      this->_power -= 25;
      std::cout << this->_name << " launches a fire ball" << std::endl;
      return (20 + this->_spirit);
    }
  else
    std::cout << this->_name << " out of power" << std::endl;
  return (0);
}

void		Mage::RestorePower()
{
  if ((this->_power + 50 + this->_intelligence) > 100)
    this->_power = 100;
  else
    this->_power += (50 + this->_intelligence);
  std::cout << this->_name << " takes a mana potion" << std::endl;
}
