//
// Warrior.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 15:29:55 2014 Jean Gravier
// Last update Thu Jan 16 22:16:26 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Warrior.hh"

Warrior::Warrior(std::string const& name, int lvl, std::string weaponName): Character(name, lvl)
{
  this->_class = "Warrior";
  this->_race = "Dwarf";
  this->_strength = 12;
  this->_stamina = 12;
  this->_intelligence = 6;
  this->_spirit = 5;
  this->_agility = 7;
  this->_weaponName = weaponName;
  std::cout << "I'm " << this->_name << " KKKKKKKKKKRRRRRRRRRRRRRREEEEEEEEOOOOOOORRRRGGGGGGG" << std::endl;
}

Warrior::~Warrior()
{

}

int	Warrior::CloseAttack()
{
  if (this->_power >= 30)
    {
      this->_power -= 30;
      std::cout << this->_name << " strikes with his " << this->_weaponName << std::endl;
      return (20 + this->_strength);
    }
  std::cout << this->_name << " out of power" << std::endl;
  return (0);
}

int	Warrior::RangeAttack()
{
  this->Range = Character::CLOSE;
  if (this->_power >= 10)
    {
      this->_power -= 10;
      std::cout << this->_name << " intercepts" << std::endl;
      return (0);
    }
  std::cout << this->_name << " out of power" << std::endl;
  return (0);
}

std::string	Warrior::getWeaponName() const
{
  return (this->_weaponName);
}
