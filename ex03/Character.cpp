//
// Character.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d09/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 16 11:04:10 2014 Jean Gravier
// Last update Thu Jan 16 22:40:51 2014 Jean Gravier
//

#include "Character.hh"
#include <iostream>
#include <string>

Character::Character(std::string const& name, int lvl): Range(Character::CLOSE), _name(name), _lvl(lvl)
{
  this->_class = "Character";
  this->_race = "Koala";
  this->_pv = 100;
  this->_power = 100;
  this->_strength = 5;
  this->_stamina = 5;
  this->_intelligence = 5;
  this->_spirit = 5;
  this->_agility = 5;
  std::cout << this->_name << " Created" << std::endl;
}

Character::~Character()
{

}

/*************
 ***GETTERS***
 ************/
std::string const&	Character::getName() const
{
  return (this->_name);
}


std::string		Character::getClass() const
{
  return (this->_class);
}

std::string		Character::getRace() const
{
  return (this->_race);
}

int			Character::getLvl() const
{
  return (this->_lvl);
}

int			Character::getPv() const
{
  return (this->_pv);
}

int			Character::getPower() const
{
  return (this->_power);
}

int			Character::getStrength() const
{
  return (this->_strength);
}

int			Character::getStamina() const
{
  return (this->_stamina);
}

int			Character::getIntelligence() const
{
  return (this->_intelligence);
}

int			Character::getSpirit() const
{
  return (this->_spirit);
}

int			Character::getAgility() const
{
  return (this->_agility);
}

/*****************
 ***END GETTERS***
 ****************/


/******************
 ***INTERACTIONS***
 *****************/

void		Character::TakeDamage(int _damage)
{
  if ((this->_pv - _damage) > 0)
    {
      this->_pv -= _damage;
      std::cout << this->_name.c_str() << " takes " << _damage << " damage" << std::endl;
    }
  else
    {
      this->_pv = 0;
      std::cout << this->_name.c_str() << " out of combat." << std::endl;
    }
}

int		Character::CloseAttack()
{
  if (this->_power >= 10)
    {
      this->_power -= 10;
      std::cout << this->_name << " strikes with a wood stick" << std::endl;
      return (10 + this->_strength);
    }
  else
    std::cout << this->_name << " out of power" << std::endl;
  return (0);
}

int		Character::RangeAttack()
{
  if (this->_power >= 10)
    {
      this->_power -= 10;
      std::cout << this->_name << " launches a stone" << std::endl;
      return (5 + this->_strength);
    }
  else
    std::cout << this->_name << " out of power" << std::endl;
  return (0);
}

void		Character::Heal()
{
  if ((this->_pv + 50) > 100)
    this->_pv = 100;
  else
    this->_pv += 50;
  std::cout << this->_name << " takes a potion" << std::endl;
}

void		Character::RestorePower()
{
  this->_power = 100;
  std::cout << this->_name << " eats" << std::endl;
}

/**********************
 ***END INTERACTIONS***
 *********************/
